import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

const TextInputGroup = ({
  label,
  name,
  type,
  value,
  placeholder,
  changed,
  error
}) => {
  // let classes = ["form-control", "form-control-lg"].join(" ");

  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        //className={error ? [classes, "is-invalid"].join(" ") : classes}
        className={classnames("form-control form-control-lg", {
          "is-invalid": error
        })}
        type={type}
        name={name}
        id={name}
        placeholder={placeholder}
        value={value}
        onChange={changed}
      />
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

TextInputGroup.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  changed: PropTypes.func.isRequired,
  error: PropTypes.string
};

export default TextInputGroup;
