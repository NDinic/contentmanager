import React from "react";

const About = () => {
  return (
    <div>
      <h1 className="display-4">About us</h1>
      <p className="lead">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatem, at
        animi. Laboriosam cupiditate eius aut, non porro, vel dolore ullam earum
        tempore rem voluptates iste molestias deleniti natus nisi magni placeat
        consequatur voluptatum commodi asperiores ea cumque maxime! Eligendi
        rerum libero nemo perspiciatis voluptatem illo ipsam vitae assumenda
        odio laudantium.
      </p>
      <p className="text-secondary">Lorem, ipsum dolor.</p>
    </div>
  );
};

export default About;
