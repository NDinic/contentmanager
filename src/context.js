import React, { Component } from 'react';
import Axios from 'axios';

const Context = React.createContext();

export class Provider extends Component {
  state = {
    contacts: []
  }
  async componentDidMount() {
    let res = await Axios.get('https://jsonplaceholder.typicode.com/users');

    this.setState({
      contacts: res.data
    })

  }
  deleteContactHandler = async (id) => {
    try{
      await Axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      this.setState({
        ...this.state,
        contacts: this.state.contacts.filter(contact => contact.id !== id)
      })
    }
    catch(e){
      this.setState({
        ...this.state,
        contacts: this.state.contacts.filter(contact => contact.id !== id)
      })
    }
    
  }
  addContactHandler = (newContact) => {
    this.setState({
      ...this.state,
      contacts: [newContact, ...this.state.contacts]
    })
  }
  updateContactHandler = (contactData) => {
    console.log(contactData)
    this.setState({
      ...this.state,
      contacts: this.state.contacts.map( contact => contact.id === contactData.id ? (contact = contactData) : contact)
    })
  }
  getState = () => {
    return {
      ...this.state,
      deleteContact: this.deleteContactHandler,
      addContact: this.addContactHandler,
      updateContact: this.updateContactHandler
    }
  }

  render(){
    return(
      <Context.Provider value={this.getState()}>
        {this.props.children}
      </Context.Provider>
    )
  }
}

export const Consumer = Context.Consumer;